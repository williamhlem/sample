function provSelectBox(){
    // Set up the array
    var provArray = ["--- Select ---", "Alberta", "British Columbia", "Manitoba", "New Brunswick", "Newfoundland and Labrador", "Nova Scotia", "Ontario", "Prince Edward Island", "Quebec", "Saskatchewan"];
    var provOption="";

    for(i=0;i<provArray.length;i++){
        provOption+="<option name='prov" + i + "' value='" + provArray[i] + "'>" + provArray[i] + "</option>";
    }

    document.getElementById("selectProv").innerHTML+=provOption;
}

function validateForm(){
    // declare Email format
    var txtEmailRegex = /\b(\w+\.)*\w+@([a-zA-Z]+\.)+[a-zA-Z]{2,6}\b/;
    
    // Name validation
    if(document.getElementById("txtName").value==""){
        alert("Please enter a Name.");
        document.getElementById("txtName").style.backgroundColor='pink';
        document.getElementById("txtName").focus();
        return false; // stop the submit of the form

    // Email validation
    } else if(document.getElementById("txtEmail").value=="" || !txtEmailRegex.test(document.getElementById("txtEmail").value)){
        alert("Please enter an Email.");
        document.getElementById("txtEmail").style.backgroundColor='pink';
        document.getElementById("txtEmail").focus();
        return false; // stop the submit of the form

    // select box validation
    } else if(document.getElementById("selectProv").selectedIndex==0){
        alert("Please select a province.");
        document.getElementById("selectProv").focus();
        return false; // stop the submit of the form
    }

    document.forms[0].submit();  // validation passed, submit the form.
    document.write("Form Submitted.");
}
