$(document).ready(function(){

    $("#playerPrompt").click(function(){

        var gamePieces = new Array("Rock", "Paper", "Scissors", "Dynamite");
        var gplength = gamePieces.length;
        var computer = "";
        var winner = "";

        // prompt player to choose rock, paper, scissors or dynamite
        var player = prompt("Please enter Rock, Paper, Scissors or Dynamite:","");

        if(player != ""){
            // changing entry to all lowercase
            player = player.toLowerCase();
            // capitalize the first letter of the word
            player = player[0].toUpperCase() + player.slice(1);
        }else{
            player = prompt("Try again. Please enter Rock, Paper, Scissors or Dynamite:","");
        }
                
        // validate entry
        switch(player){
            case "Rock":
                alert("Player selected: Rock");
                break;
            case "Paper":
                alert("Player selected: Paper");
                break;
            case "Scissors":
                alert("Player selected: Scissors");
                break;
            case "Dynamite":
                alert("Player selected: Dynamite");
                break;
            default:
                alert("Player selected: " + player + " ???");
                break;
        }

        // Assign gamepiece to computer
        function getRandomGamePiece(arraySize){
            var computerSelect = gamePieces[Math.floor(Math.random() * arraySize)];
                
            switch(computerSelect){
                case "Rock":
                    alert("Computer selected: Rock");
                    break;
                case "Paper":
                    alert("Computer selected: Paper");
                    break;
                case "Scissors":
                    alert("Computer selected: Scissors");
                    break;
                case "Dynamite":
                    alert("Computer selected: Dynamite");
                    break;
                default:
                    break;
            }
            return computerSelect;     
        }

        computer = getRandomGamePiece(gplength);

        var playerVersusComputer = player+computer          
                 
        function whoWins(compare){
            var winner1 = "";

            switch (compare){
                case "RockPaper":
                    winner1 = "Computer";
                    break;
                case "RockScissors":
                    winner1 = "Player";
                    break;
                case "RockDynamite":
                    winner1 = "Computer";
                    break;

                case "PaperRock":
                    winner1 = "Player";
                    break;
                case "PaperScissors":
                    winner1 = "Computer";
                    break;
                case "PaperDynamite":
                    winner1 = "Computer";
                    break;
                    
                case "ScissorsRock":
                    winner1 = "Computer";
                    break;
                case "ScissorsPaper":
                    winner1 = "Player";
                    break;
                case "ScissorsDynamite":
                    winner1 = "Player";
                    break;

                case "DynamiteRock":
                    winner1 = "Player";
                    break;
                case "DynamitePaper":
                    winner1 = "Player";
                    break;
                case "DynamiteScissors":
                    winner1 = "Computer";
                    break;

                case "RockRock":
                    winner1 = "Tie, no one";
                    break;
                case "PaperPaper":
                    winner1 = "Tie, no one";
                    break;
                case "ScissorsScissors":
                    winner1 = "Tie, no one";
                    break;
                case "DynamiteDynamite":
                    winner1 = "Tie, no one";
                    break;

                default:
                    winner1 = "Player forfeit.  Computer "
                    break;
            }
            return winner1;
        }

        winner = whoWins(playerVersusComputer);

        $("#playerPiece").text("Player selected: " + player);
        $("#computerPiece").text("Computer selected: " + computer);
        $("#results").text(winner + " wins!!!");
    });
});    